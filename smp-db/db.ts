import { SafeMessagePack } from "../safe-msg-pack.ts";
import { ensureDir } from "https://deno.land/std@0.190.0/fs/ensure_dir.ts";
import { fsGeneticInterface as fsGenInt, fsPolymorphAuto } from "./polymorph/fs-polymorph.ts";
import { SMP_Store, openOrMakeStore, openStore, makeStore } from "./data-store.ts";
export abstract class fsGeneticInterface extends fsGenInt {}
export { FS_METHODS } from "./polymorph/fs-polymorph.ts";
function ssg(g: unknown): g is Set<string> {
    if(g instanceof Set){
        for(const e of g){
            if(typeof e != "string")
                return false;
        }
        return true;
    }
    return false;
}

function unknownGuard(_u: unknown): _u is unknown {
    return true;
}

export class SMP_DB {
    private static tableIndexName = "__storeIndex";
    private static cleanupIndexName = "__cleanupIndex";
    private static fileExt = "smpk"
    private readonly fs: fsGenInt;
    private readonly folder: string;
    private readonly stores: SMP_Store<Set<string>>;
    private readonly cleanupList: SMP_Store<Set<string>>;
    protected readonly smp: SafeMessagePack;
    private constructor(
        folder: string,
        storesIndex: SMP_Store<Set<string>>,
        cleanupIndex: SMP_Store<Set<string>>,
        smp: SafeMessagePack,
        fs: fsGenInt
        ) {
        this.folder = folder;
        this.stores = storesIndex;
        this.cleanupList = cleanupIndex;
        this.smp = smp;
        this.fs = fs;
    }

    private static async getOrMakeStoreIndex(
        folder: string,
        smp: SafeMessagePack,
        fs: fsGenInt
        ): Promise<SMP_Store<Set<string>>> {

        const filePath = `${folder}/${SMP_DB.tableIndexName}.${SMP_DB.fileExt}`;
        try{
            const indexStore = await openOrMakeStore<Set<string>>(
                filePath, new Set<string>(), smp, fs, ssg
            );
            return indexStore;
        } catch(e) {
            if(e instanceof SMP_Store.errors.TypeMissmatchError){
                throw new Error("Corroupt store Index");
            } else {
                throw e;
            }
        }
    }

    private static async getOrMakeCleanupIndex(
        folder: string,
        smp: SafeMessagePack,
        fs: fsGenInt
        ): Promise<SMP_Store<Set<string>>> {
        const filePath = `${folder}/${SMP_DB.cleanupIndexName}.${SMP_DB.fileExt}`;
        try{
            const cleanupStore = await openOrMakeStore<Set<string>>(
                filePath, new Set<string>(), smp, fs, ssg
            );
            return cleanupStore;
        } catch(e) {
            if(e.message == `Type missmatch on table ${filePath}`){
                throw new Error("Corroupt cleanup Index");
            } else {
                throw e;
            }
        }
    }

    static async init(
        folder: string,
        readonly: boolean,
        fsParam?: fsGenInt
        ) {
        const fs = fsParam ?? await fsPolymorphAuto(readonly);
        await ensureDir(folder);
        const smp = new SafeMessagePack();
        const [storeIndex, cleanupIndex] = await Promise.all([
            SMP_DB.getOrMakeStoreIndex(folder, smp, fs),
            SMP_DB.getOrMakeCleanupIndex(folder, smp, fs)
        ]);
        return new SMP_DB(folder, storeIndex, cleanupIndex, smp, fs);
    }

    private async updateIndex() {
        await Promise.all([
            this.stores.flush(),
            this.cleanupList.flush(),
        ]);
    }

    public async newStore<T>(
        name: string,
        baseData: T,
        guard: (g:unknown) => g is T,
        force = false
        ): Promise<SMP_Store<T> | null> {
        if(this.stores.data.has(name) && force == false){
            return null;
        }
        this.stores.data.add(name);
        await this.updateIndex();
        const [store, _update] = await Promise.all([
            makeStore<T>(
            `${this.folder}/${name}.${SMP_DB.fileExt}`,
            baseData, this.smp, this.fs, guard
            ),
            this.updateIndex()
        ]);
        await store.flush();
        return store;
    }

    public async newOrOpenAndRepairStore<T>(
        name: string,
        fallbackData: T,
        guard: (g:unknown) => g is T,
        repair: (g:unknown) => T,
        force = false,
        ): Promise<SMP_Store<T>>{
        if(this.cleanupList.data.has(name)){
            await this.deleteStore(name);
            this.cleanupList.data.delete(name);
            this.updateIndex();
        }
        if(this.stores.data.has(name) && force == false){
            return await this.openRepairStore<T>(name, guard, repair);
        }
        const store = await this.newStore(
            name,
            fallbackData,
            guard,
            true,
        );
        if(store){
            return store;
        }
        throw new Error("Unreachable Line.");
    }

    public async newOrOpenStore<T>(
        name: string,
        fallbackData: T,
        guard: (g:unknown) => g is T,
        force = false,
        ): Promise<SMP_Store<T>>{
        if(this.cleanupList.data.has(name)){
            await this.deleteStore(name);
            this.cleanupList.data.delete(name);
            this.updateIndex();
        }
        if(this.stores.data.has(name) && force == false){
            return await this.openStore<T>(name, guard);
        }
        const store = await this.newStore(
            name,
            fallbackData,
            guard,
            true,
        );
        if(store){
            return store;
        }
        throw new Error("Unreachable Line.");
    }

    async openRepairStore<T>(
        name: string,
        guard: (g: unknown) => g is T,
        repair: (g: unknown) => T,
    ): Promise<SMP_Store<T>> {
        const tmpStore = await openStore<unknown>(
            `${this.folder}/${name}.${SMP_DB.fileExt}`,
            this.smp,
            this.fs,
            (g: unknown): g is unknown=>{return !!g}
            );
        tmpStore.data = repair(tmpStore.data);
        await tmpStore.flush();
        return openStore<T>(
            `${this.folder}/${name}.${SMP_DB.fileExt}`,
            this.smp,
            this.fs,
            guard
            );
    }

    public openStore<T>(
        name: string,
        guard: (g: unknown) => g is T
        ): Promise<SMP_Store<T>> {
        return openStore<T>(
            `${this.folder}/${name}.${SMP_DB.fileExt}`,
            this.smp,
            this.fs,
            guard
            );
    }

    private deleteStore(name: string) {
        return this.fs.remove(`${this.folder}/${name}.${SMP_DB.fileExt}`);
    }

    public dropStore(name: string) {
        if(this.stores.data.has(name)){
            this.cleanupList.data.add(name);
            this.stores.data.delete(name);
        }
    }

    public async cleanup() {
        const waitlist: Promise<unknown>[] = []
        for(const name of this.cleanupList.data){
            waitlist.push(this.deleteStore(name));
        }
        this.cleanupList.data.clear();
        waitlist.push(this.updateIndex());
        await Promise.all(waitlist);
    }

    public flush() {
        return this.updateIndex();
    }

    public async diagDump() {
        await Promise.all([
            this.cleanupList.dump(),
            this.stores.dump(),
        ]);
    }

    public *cycleStoreNames(): Generator<string, void> {
        for(const store of this.stores.data){
            yield(store);
        }
    }
    public async diagDumpFull() {
        for(const storeName of this.cycleStoreNames()){
            try {
                const store = await this.openStore<unknown>(storeName, unknownGuard);
                await store.dump();
            } catch {
                continue;
            }
        }
        await this.diagDump();
    }
}