import { SafeMessagePack } from "../safe-msg-pack.ts";
import { unsafeJSON } from "./json-dumper.ts"
import { fsGeneticInterface as fsGenInt } from "./polymorph/fs-polymorph.ts";
export async function openOrMakeStore<T>(
    path: string,
    seedData: T,
    smp: SafeMessagePack,
    fs: fsGenInt,
    guard: (g:unknown) => g is T
): Promise<SMP_Store<T>> {
    try {
        return await openStore<T>(path, smp, fs, guard);
    } catch(e) {
        if(e instanceof fsGenInt.errors.NotFound){
            return makeStore<T>(path, seedData, smp, fs, guard);
        }
        throw e;
    }
}

export async function makeStore<T>(
    path: string,
    seedData: T,
    smp: SafeMessagePack,
    fs: fsGenInt,
    guard: (g:unknown) => g is T
): Promise<SMP_Store<T>> {
    const store = new SMP_Store<T>(
        path,
        seedData,
        smp,
        fs,
        guard
        );
    await store.flush();
    return store;
}

export async function openStore<T>(
    filePath: string,
    smp: SafeMessagePack,
    fs: fsGenInt,
    guard: (g:unknown) => g is T
): Promise<SMP_Store<T>> {
        return new SMP_Store<T>(
            filePath,
            await loadData<T>(filePath, smp, fs, guard),
            smp,
            fs,
            guard
            );
}

export async function loadData<T>(
    filePath: string,
    smp: SafeMessagePack,
    fs: fsGenInt,
    guard: (g:unknown) => g is T
) {
    const fileStream = await fs.readFile(filePath);
    const data = smp.decode(fileStream);
    if(guard(data)){
        return data;
    } else {
        throw new SMP_Store.errors.TypeMissmatchError(filePath, guard.name, "loadData");
    }
}

export class TypeMissmatchError extends Error {
    readonly path: string;
    constructor(path: string, guard: string, event?: string) {
        let msg;
        if(event){
            msg = `Type missmatch on '${path}' (Guard Fail FN:${guard}) during ${event}`;
        } else{
            msg = `Type missmatch on '${path}' (Guard Fail FN:${guard})`;
        }
        super(msg);
        this.path = path;
    }
}
export class SMP_Store<T> {
    private readonly file: string;
    private readonly smp: SafeMessagePack;
    private store: T;
    private readonly fs: fsGenInt;
    private readonly guard: (g:unknown) => g is T
    constructor(
        file: string,
        store: T,
        smp: SafeMessagePack,
        fs:fsGenInt,
        guard: (g:unknown) => g is T
        ) {
        this.file = file;
        this.smp = smp;
        this.store = store;
        this.fs = fs;
        this.guard = guard;
    }
    flush() {
        return this.fs.writeFile(
            this.file,
            this.smp.encode(this.store)
        );
    }

    flushSafe() {
        const encoded = this.smp.encode(this.data)
        const schrodinger = this.smp.decode(encoded);
        const a = this.guard(schrodinger);
        const b = this.data == schrodinger;
        if(a&&b)
            return this.fs.writeFile(this.file, encoded);
        else
            throw new SMP_Store.errors.TypeMissmatchError(
                this.file,
                this.guard.name,
                "flushSafe"
            )
    }

    dump() {
        const str = JSON.stringify(
            this.store,
            unsafeJSON,
            2
        );
        const uint8 = new Uint8Array(str.length);
        for (let i = 0; i < str.length; i++) {
          uint8[i] = str.charCodeAt(i);
        }
        return this.fs.writeFile(
            `${this.file}.json`,
            uint8
        )
    }

    /**
     * Verifies encodeing safety by attempting to encode then decode the data.
     * @returns true if it is safe to encode.
     */
    verifyStoreSafety(): boolean {
        const schrodinger = this.smp.decode(
            this.smp.encode(this.data)
        );
        const a = this.guard(schrodinger);
        const b = this.data == schrodinger;
        return a && b;
    }
    async refresh() {
        this.data = await loadData<T>(this.file, this.smp, this.fs, this.guard);
    }

    get data() {
        return this.store;
    }

    set data(data: T) {
        this.store = data;
    } 
    static errors = {
        TypeMissmatchError
    }
}