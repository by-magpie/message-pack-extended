
// deno-lint-ignore no-explicit-any
export function unsafeJSON(key:string, data: any):
  undefined | null | number | string | boolean | Array<unknown> | Record<string, unknown> {
    switch(typeof data){
      case "string":
        return data;
      case "number":
        if(data == Infinity){
          return "Infinity";
        }
        if(data == -Infinity){
          return "-Infinity"
        }
        return data;
      case "bigint":
        return data.toString();
      case "boolean":
        return data;
      case "symbol":
        throw new Error("I don't understand symbols yet.");
      case "undefined":
        return undefined;
      case "function":
        throw `Function named ${key} goes here.`;
    }
    if(Array.isArray(data)){
      return data;
    }
    if(data instanceof Map){
      // deno-lint-ignore no-explicit-any
      const r: Record<any, any> = {};
      for(const [k, v] of data){
        r[k] = v;
      }
      return r;
    }
    if(data instanceof Set){
      return [...data];
    }
    if(data instanceof Date){
      return data.toString();
    }
    if(typeof data == "object"){
        return data;
    }
    if(isNaN(data)){
      return "NaN";
    }
    return data;
}