import {default as fsGeneticInterfaceInt} from "./fs-poly-files/fs-generic.ts";
export abstract class fsGeneticInterface extends fsGeneticInterfaceInt {}

export enum FS_METHODS {
    DENO,
    HTTP,
    LOCALSTORAGE,
    IPFS,
} 
export function fsPolymorphAuto(readonly = false): Promise<fsGeneticInterfaceInt> {
    let opt: FS_METHODS;
    if("Deno" in window){
        opt = FS_METHODS.DENO;
    } else {
        if(readonly) {
            opt = FS_METHODS.HTTP;
        } else {
            opt = FS_METHODS.LOCALSTORAGE;
        }
    }
    return fsPolymorphManual(opt, readonly);
}

export async function fsPolymorphManual(option: FS_METHODS, readonly = false): Promise<fsGeneticInterfaceInt> {
    // deno-lint-ignore no-explicit-any
    let imp: any;
    switch(option){
        case FS_METHODS.DENO:
            imp = await import("./fs-poly-files/fs-deno.ts");
            break;
        case FS_METHODS.HTTP:
            imp = await import("./fs-poly-files/fs-http-fetch.ts");
            break;
        case FS_METHODS.LOCALSTORAGE:
            imp = await import("./fs-poly-files/fs-http-localstorage.ts");
            break;
        case FS_METHODS.IPFS:
            imp = await import("./fs-poly-files/fs-ipfs.ts");
            break;
    }
    if(imp.default){
        return new imp.default(readonly);
    } else {
        throw new Error("Missing default import!");
    }
}