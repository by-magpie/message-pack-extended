class NotFound extends Error {
    public readonly path: string;
    constructor(path: string, cause: Error, additional?: string) {
        let msg: string;
        if(additional){
            msg = `File ${path} Not found.`;
        } else {
            msg = `File ${path} Not found.`;
        }
        super(msg, {cause});
        this.path = path;
    }

}
class PermissionDenied extends Error {
    public readonly path: string;
    constructor(
        action: string,
        path: string,
        cause: Error,
        additional?: string
    ) {
        let msg: string;
        if(additional){
            msg = `Insufficent premissions to ${action} ${path}. (${additional})`;
        } else {
            msg = `Insufficent premissions to ${action} ${path}.`;
        }
        super(msg, {cause});
        this.path = path;
    }

}

export default abstract class fsGeneticInterface {
    readonly write: boolean;
    constructor(readonly: boolean){
        this.write = !readonly;
    }
    abstract readFile(uri: string):  Promise<Uint8Array>;
    abstract writeFile(uri: string, data: Uint8Array): Promise<true | false>;
    abstract remove(uri: string, recursive?: boolean): Promise<true | false>;

    public static errors = {
        NotFound,
        PermissionDenied
    }
}