import fsGeneticInterface from "./fs-generic.ts";
export default class WEB_FS_RW extends fsGeneticInterface {
    readFile(_uri: string): Promise<Uint8Array> {
        throw new Error("Method not implemented.");
    }
    writeFile(_uri: string, _data: Uint8Array): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    remove(_uri: string, _recursive?: boolean|undefined): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
}
