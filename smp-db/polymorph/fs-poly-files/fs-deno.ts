import fsGeneticInterface from "./fs-generic.ts";
export default class DENO_FS_RW extends fsGeneticInterface {
    async readFile(uri: string): Promise<Uint8Array> {
        try{
            return await Deno.readFile(uri);
        } catch(e) {
            if(e instanceof Deno.errors.NotFound){
                throw new fsGeneticInterface.errors.NotFound(uri, e);
            }
            if(e instanceof Deno.errors.PermissionDenied){
                throw new fsGeneticInterface.errors.PermissionDenied("read", uri, e);
            }
            throw e;
        }
    }
    async writeFile(uri: string,data: Uint8Array): Promise<boolean> {
        if(!this.write){
            return false;
        }
        try {
            await Deno.writeFile(uri, data);
            return true;
        } catch(e) {
            if(e instanceof Deno.errors.PermissionDenied){
                throw new fsGeneticInterface.errors.PermissionDenied("write", uri, e);
            }
            throw e;
        }
    }
    async remove(uri: string,recursive = false): Promise<boolean> {
        if(!this.write){
            return false;
        }
        try {
            await Deno.remove(uri, {recursive});
            return true
        } catch (e) {
            if(e instanceof Deno.errors.NotFound){
                throw new fsGeneticInterface.errors.NotFound(uri, e);
            }
            if(e instanceof Deno.errors.PermissionDenied){
                if(recursive){
                    throw new fsGeneticInterface.errors.PermissionDenied("delete recursively", uri, e);
                } else {
                    throw new fsGeneticInterface.errors.PermissionDenied("delete", uri, e);
                }
            }
            if(e.code == "ENOTEMPTY"){
                throw new fsGeneticInterface.errors.PermissionDenied("delete recursively", uri, e, "Not Recursive & Not Empty");
            }
            throw e;
        }
    }
}