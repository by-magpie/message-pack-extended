# message-pack-extended

This is for keeping all my message pack stuff contained because it's such a useful utility.

## MapSafe
MapSafe is a collection of utilites for safely encoding and decoding JS Map objects because *of course* we can't save maps properly.

## MPDB
MPDB is a "I'm too lazy to use a proper DB" Database. It's just a folder of .mpk files containing the "stores" ("tables") of the DB.

It's a lazy data check. Stores can be invalid for the data-type.