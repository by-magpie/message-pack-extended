export { fsGeneticInterface as fsGenInt } from "./smp-db/polymorph/fs-polymorph.ts";
export { FS_METHODS } from "./smp-db/polymorph/fs-polymorph.ts";
export { SMP_DB } from "./smp-db/db.ts";
export { SMP_Store } from "./smp-db/data-store.ts";